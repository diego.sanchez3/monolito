package com.example.demo.repositories;

import java.util.ArrayList;

import com.example.demo.models.ImageModelMongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageMongoRepository extends MongoRepository<ImageModelMongo, Long> {
    @Query("{_id: {$in : ?0}}")
    public abstract ArrayList<ImageModelMongo> findAllByIds(ArrayList<Long> ids);
}
