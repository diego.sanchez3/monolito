package com.example.demo.repositories;

import com.example.demo.models.ImageModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends CrudRepository<ImageModel, Long> {
    
}
