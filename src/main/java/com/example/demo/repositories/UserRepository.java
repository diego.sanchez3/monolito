package com.example.demo.repositories;

import com.example.demo.models.UserModel;
import java.util.Optional;
import java.util.ArrayList;

// import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Long> {
    // @Query(value = "SELECT * FROM user u WHERE u.type = ?1 and u.id = ?2", nativeQuery = true)
    // public Optional<UserModel> findByDocumentTypeAndDocumentNumber(String documentType, Long documentNumber);
    public abstract Optional<UserModel> findByDocumentTypeAndDocumentNumber(String documentType, Long documentNumber);
    // @Query(value = "SELECT * FROM user u WHERE u.age >= ?1", nativeQuery = true)
    public  abstract ArrayList<UserModel> findByAgeGreaterThanEqual(Integer age);
}
