package com.example.demo.controllers;
import java.util.Optional;

import com.example.demo.models.ImageModel;
import com.example.demo.services.ImageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/image")
public class ImageController {
    
    @Autowired
    private ImageService imageService;

    @GetMapping(path = "/{id}")
    public Optional<ImageModel> getImage(@PathVariable Long id) {
        return imageService.getImage(id);
    }
    @PostMapping(path = "/addImage")
    public ImageModel addImage(@RequestParam("id") Long id, @RequestParam("file") MultipartFile file) {
        return imageService.addImage(file);
    }
    @PutMapping(path = "/updateImage")
    public ImageModel updateImage(@RequestParam("id") Long id, @RequestParam("file") MultipartFile file) {
        return imageService.addImage(file);
    }
    @DeleteMapping(path = "/{id}")
    public boolean deleteImage(@PathVariable Long id){
        return imageService.deleteImage(id);
    }
}
