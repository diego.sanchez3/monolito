package com.example.demo.controllers;

import com.example.demo.models.ImageModelMongo;
import com.example.demo.models.UserModel;
import com.example.demo.services.ImageServiceMongo;
import com.example.demo.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;
import java.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

// import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/operations")
public class TransationsController {
    
    @Autowired
    private ImageServiceMongo imageService;
    @Autowired
    private UserService userService;

    @PostMapping(path = "/create-user")
    public ResponseEntity<Object> createUser(@RequestPart("user") String user, @RequestPart("file") MultipartFile file) throws IOException{
        UserModel userM = new UserModel();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            userM = objectMapper.readValue(user, UserModel.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        UserModel user_new = userService.addUser(userM);
        Long idPhoto = user_new.getDocumentNumber();
        ImageModelMongo image = imageService.addImage(idPhoto, file);
        Map<String, Object> merged = new HashMap<String, Object>();
        merged.put("user", user_new);
        merged.put("photo", image);
        return new ResponseEntity<Object> (merged, HttpStatus.OK);
    }
    @GetMapping(path = "/user/findUser")
    public ResponseEntity<Object> findUser(@RequestParam("id") Long id, @RequestParam("type") String type){
        Optional<UserModel> user = userService.findUser(id, type);
        Long imageId = user.get().getDocumentNumber();
        Optional<ImageModelMongo> image = imageService.getImage(imageId);
        Map<String,Object> data = new HashMap<String,Object>();
        data.put("user", user);
        data.put("image", image);
        return new ResponseEntity<Object> (data, HttpStatus.OK);
    }
    @GetMapping(path = "/user")
    public ArrayList<Map<String,Object>> findByGreaterAge(@RequestParam("age") Integer age) {
        ArrayList<UserModel> users = userService.findByGreaterAge(age);
        ArrayList<Long> ids = new ArrayList<Long>();
        users.stream().forEach(user -> ids.add(user.getDocumentNumber()));
        ArrayList<ImageModelMongo> images = imageService.getByIds(ids);
        ArrayList<Map<String,Object>> result = new ArrayList<Map<String,Object>> () ;
        for (int i = 0; i < users.size(); i++) {
            Map<String,Object> data = new HashMap<String,Object>();
            data.put("user", users.get(i));
            // data.put("image", images.size());
            data.put("image", images.get(i));
            result.add(data);
        }
        return result;
    }
    @DeleteMapping(path = "/deleteUser")
    public boolean deleteUser(@RequestParam("id") Long id){
        boolean deleteUserCheck = userService.deleteUserById(id);
        boolean deleteImageCheck = imageService.deleteImage(id);
        return deleteUserCheck && deleteImageCheck;
    }
    
}
