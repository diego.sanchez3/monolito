package com.example.demo.controllers;

import com.example.demo.models.UserModel;
import com.example.demo.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Optional;


@RestController
@RequestMapping("/users")
public class UserController {
    
    @Autowired
    private UserService userService;
    
    @GetMapping()
    public ArrayList<UserModel> getUsers() {
        return userService.getUsers();
    }
    @GetMapping(path = "/{id}")
    public Optional<UserModel> getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }
    @PostMapping()
    public UserModel addUser(@RequestBody UserModel model) {
        return userService.addUser(model);
    }
    @PutMapping()
    public UserModel updateUser(@RequestBody UserModel model) {
        return userService.updateUser(model);
    }
    @DeleteMapping(path = "/{id}")
    public boolean deleteUser(@PathVariable Long id) {
        return userService.deleteUserById(id);
    }

    
}
