package com.example.demo.services;

import com.example.demo.models.ImageModel;
import com.example.demo.repositories.ImageRepository;
import java.util.Optional;
import java.io.IOException;
import java.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageService {
   
    @Autowired
    private ImageRepository imageRepository;

    public ImageModel addImage(MultipartFile file){

        ImageModel image = new ImageModel();
        try {
            image.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageRepository.save(image);
    }
    public ImageModel updateImage(Long id, MultipartFile file){

        ImageModel image = new ImageModel();
        image.setId(id);
        try {
            image.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageRepository.save(image);
    }

    public Optional<ImageModel> getImage(Long id){
        return imageRepository.findById(id);
    }

    public boolean deleteImage(Long id){
        try {
            imageRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
}
