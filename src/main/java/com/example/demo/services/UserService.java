package com.example.demo.services;

import java.util.ArrayList;
import java.util.Optional;

import com.example.demo.models.UserModel;
import com.example.demo.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
   
    public UserService() {

    }

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    private UserRepository userRepository;

    public ArrayList<UserModel> getUsers() {
        return (ArrayList<UserModel>) userRepository.findAll();
    }
    public Optional<UserModel> getUser(Long userId) {
        return userRepository.findById(userId);
    }
    public UserModel addUser(UserModel user) {
        return userRepository.save(user);
    }
    public UserModel updateUser(UserModel user){
        return userRepository.save(user);
    }
    public boolean deleteUserById(Long userId){
        Optional<UserModel> user = userRepository.findById(userId);
        if (user.isPresent()){ 
            userRepository.deleteById(userId);
            return true;
        }
        else{
            return false;
        }
    }
    public Optional<UserModel> findUser(Long id, String type){
        return userRepository.findByDocumentTypeAndDocumentNumber(type, id);
    }
    public ArrayList<UserModel> findByGreaterAge(Integer age){
        return userRepository.findByAgeGreaterThanEqual(age);
    }
}
