package com.example.demo.services;

import com.example.demo.models.ImageModelMongo;
import com.example.demo.repositories.ImageMongoRepository;
import java.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageServiceMongo {
   
    @Autowired
    private ImageMongoRepository imageRepository;
    

    public ImageServiceMongo() {
    }
    public ImageServiceMongo(ImageMongoRepository imageRepository) {
        this.imageRepository = imageRepository;
    }
    public ImageModelMongo addImage(Long id, MultipartFile file) throws IOException{

        ImageModelMongo image ;
        image = new ImageModelMongo(Base64.getEncoder().encodeToString(file.getBytes()));
        image.setId(id);
        return imageRepository.save(image);
    }
    public ImageModelMongo updateImage(Long id, MultipartFile file) throws IOException{

        ImageModelMongo image ;
        image = new ImageModelMongo(Base64.getEncoder().encodeToString(file.getBytes()));
        image.setId(id);
        return imageRepository.save(image);
    }

    public Optional<ImageModelMongo> getImage(Long id){
        return imageRepository.findById(id);
    }

    public boolean deleteImage(Long id){
        try {
            imageRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ArrayList<ImageModelMongo> getByIds(ArrayList<Long> ids){
        return imageRepository.findAllByIds(ids);   
    }
    
}

