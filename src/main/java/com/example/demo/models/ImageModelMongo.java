package com.example.demo.models;

import javax.persistence.Id;
import javax.persistence.Lob;
import org.springframework.data.mongodb.core.mapping.Document;

// @Entity
// @Table(name = "profile_image")
@Document(collection = "images")
public class ImageModelMongo  {
    
    @Id
    private Long id;

    @Lob
    private String image;

    public ImageModelMongo() {
    }

    public ImageModelMongo(Long id, String image) {
        this.id = id;
        this.image = image;
    }

    public ImageModelMongo(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}

