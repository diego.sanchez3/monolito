package com.example.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class UserModel {
   
    private String name;
    private String lastName;
    private Integer age;
    @Column(name = "born_city")
    private String bornCity;
    @Column(name = "type")
    private String documentType;
    @Id
    @Column(name = "id")
    private Long documentNumber;

    public UserModel(){

    }

    public UserModel(String name, String lastName, Integer age, String bornCity, String documentType,
            Long documentNumber) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.bornCity = bornCity;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public String getBornCity() {
        return bornCity;
    }
    public void setBornCity(String bornCity) {
        this.bornCity = bornCity;
    }
    public String getDocumentType() {
        return documentType;
    }
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
    public Long getDocumentNumber() {
        return documentNumber;
    }
    public void setDocumentNumber(Long documentNumber) {
        this.documentNumber = documentNumber;
    }
    
}
