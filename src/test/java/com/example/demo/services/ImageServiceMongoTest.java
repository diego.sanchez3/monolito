package com.example.demo.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;
import java.util.Optional;

import com.example.demo.models.ImageModelMongo;
import com.example.demo.repositories.ImageMongoRepository;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ImageServiceMongoTest {
    
    @Mock
    ImageMongoRepository imageMongo;

    @InjectMocks
    ImageServiceMongo imageService;
    
    private ImageModelMongo image;    

    public static MultipartFile createImage()throws Exception {
        File file = new File("src/main/resources/profile-pic.png");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("profile-pic.png", file.getName(),
                                                "image/png", IOUtils.toByteArray(input));
        return image;
    }
    
    public static MultipartFile createImage2()throws Exception {
        File file = new File("src/main/resources/foto.jpg");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("foto.png", file.getName(),
                                                "image/jpg", IOUtils.toByteArray(input));
        return image;
    }
    @BeforeEach
    void setUp() throws Exception {
        MultipartFile file = createImage();
        String imageEncode = Base64.getEncoder().encodeToString(file.getBytes());
        this.image = new ImageModelMongo(123L, imageEncode);
        this.imageService =  Mockito.spy(new ImageServiceMongo(this.imageMongo));
    }

    @Test
    void testAddImage() throws Exception {
        MultipartFile file = createImage();
        // Mockito.when(imageService.addImage(123L, file)).thenReturn(image);
        Mockito.doReturn(image).when(imageService).addImage(123L, file);
        ImageModelMongo response = imageService.addImage(123L, file);
        String imageEncode = Base64.getEncoder().encodeToString(file.getBytes()); 
        assertEquals(123L, response.getId());
        assertEquals(imageEncode, response.getImage());
    }

    @Test
    void testDeleteImage() {
        Mockito.doReturn(true).when(imageService).deleteImage(123L);
        boolean response = imageService.deleteImage(123L);
        assertTrue(response);
    }

    @Test
    void testGetImage() {
        Mockito.when(imageService.getImage(123L)).thenReturn(Optional.of(image));
        ImageModelMongo response = imageService.getImage(123L).get();
        assertEquals(image.getId(), response.getId());
        assertEquals(image.getImage(), response.getImage());
    }

    @Test
    void testUpdateImage() throws Exception {
        MultipartFile file = createImage2();
        ImageModelMongo customImage = image;
        customImage.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
        // Mockito.when(imageService.updateImage(123L, file)).thenReturn(customImage);
        Mockito.doReturn(customImage).when(imageService).updateImage(123L, file);
        ImageModelMongo response = imageService.updateImage(123L, file);
        assertEquals(Base64.getEncoder().encodeToString(file.getBytes()), response.getImage());

    }
}
