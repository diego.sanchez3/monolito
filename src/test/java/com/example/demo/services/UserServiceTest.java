package com.example.demo.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Optional;

import com.example.demo.models.UserModel;
import com.example.demo.repositories.UserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserServiceTest {
    
    @Mock
    private UserRepository userRepo;

    // @Autowired
    @InjectMocks
    private UserService userService;

    private UserModel testUser1;
    private UserModel testUser2;

    @BeforeEach
    void setUp(){
        this.userService = Mockito.spy(new UserService(this.userRepo));
        this.testUser1 = new UserModel("test","testeo",18,"testcity","cc",123L);
        this.testUser2 = new UserModel("test","testeo",19,"testcity","cc",1234L);
    }
    
    @Test
    void testAddUser() {
        Mockito.when(userService.addUser(testUser1)).thenReturn(testUser1);
        UserModel userAdded = userService.addUser(testUser1);    
        assertEquals("test", userAdded.getName());
        assertEquals("testeo", userAdded.getLastName());
        assertEquals(18, userAdded.getAge());
        assertEquals("testcity", userAdded.getBornCity());
        assertEquals("cc", userAdded.getDocumentType());
        assertEquals(123L, userAdded.getDocumentNumber());
    }

    @Test
    void testDeleteUser() {
        boolean result = true;
        Mockito.doReturn(result).when(userService).deleteUserById(123L);
        boolean deleteUser = userService.deleteUserById(123L);
        assertTrue(deleteUser);
    }

    @Test
    void testFindByGreaterAge() {
        ArrayList<UserModel> users = new ArrayList<UserModel>();
        users.add(testUser1);
        users.add(testUser2);
        Mockito.when(userService.findByGreaterAge(18)).thenReturn(users);
        ArrayList<UserModel> response = userService.findByGreaterAge(18);
        assertEquals(2, response.size());
        assertTrue(response.stream().anyMatch(user -> user.getDocumentNumber() == 1234L));

    }

    @Test
    void testFindUser() {
        Mockito.when(userService.findUser(testUser2.getDocumentNumber(), testUser2.getDocumentType())).thenReturn(Optional.of( testUser2));
        UserModel response = userService.findUser(testUser2.getDocumentNumber(), testUser2.getDocumentType()).get();
        assertEquals(19, response.getAge());
        assertEquals(1234L, response.getDocumentNumber());
    }


    @Test
    void testUpdateUser() {
        UserModel customTestUser1 = testUser1;
        customTestUser1.setName("victor");
        Mockito.when(userService.updateUser(testUser1)).thenReturn(customTestUser1);
        UserModel response = userService.updateUser(customTestUser1);
        assertEquals("victor", response.getName());

    }
}
