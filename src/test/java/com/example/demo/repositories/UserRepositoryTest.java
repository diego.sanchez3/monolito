package com.example.demo.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Optional;

import com.example.demo.models.UserModel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class UserRepositoryTest {
    
    @Mock
    private UserRepository userRepository;
    
    private UserModel testUser1;
    private UserModel testUser2;

    @BeforeEach
    void setUp(){
        this.testUser1 = new UserModel("test","testeo",18,"testcity","cc",123L);
        this.testUser2 = new UserModel("test","testeo",19,"testcity","cc",1234L);
    }

    @Test
    void testFindByAgeGreaterThanEqual() {
        ArrayList<UserModel> users = new ArrayList<UserModel>();
        users.add(testUser1);
        users.add(testUser2);
        Mockito.when(userRepository.findByAgeGreaterThanEqual(16)).thenReturn(users);
        ArrayList<UserModel> response = userRepository.findByAgeGreaterThanEqual(16);
        assertEquals(2, response.size());
        // assertEquals("diego", users.get(1).getName());
        assertTrue(response.stream().anyMatch(x -> x.getName().equals("test")));
    
    }

    @Test
    void testFindByDocumentTypeAndDocumentNumber() {
        Mockito.when(userRepository.findByDocumentTypeAndDocumentNumber("cc",123L)).thenReturn(Optional.of(testUser1));
        Optional<UserModel> user = userRepository.findByDocumentTypeAndDocumentNumber("cc", 123L);
        assertEquals("test", user.get().getName());
        assertEquals("testeo", user.get().getLastName());
        assertEquals("cc", user.get().getDocumentType());
        assertEquals(123L, user.get().getDocumentNumber());
        assertEquals(18, user.get().getAge());
        assertEquals("testcity", user.get().getBornCity());
    }
}
