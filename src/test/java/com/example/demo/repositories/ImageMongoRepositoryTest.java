package com.example.demo.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;
import java.util.Optional;

import com.example.demo.models.ImageModelMongo;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;


@ExtendWith(MockitoExtension.class)
public class ImageMongoRepositoryTest {

    @Mock
    ImageMongoRepository imageMongo;


    private ImageModelMongo image;    
    
    @BeforeEach
    void setUp() throws Exception {
        MultipartFile file = createImage();
        String imageEncode = Base64.getEncoder().encodeToString(file.getBytes());
        this.image = new ImageModelMongo(123L, imageEncode);
    }
    
    public static MultipartFile createImage()throws Exception {
        File file = new File("src/main/resources/profile-pic.png");
        FileInputStream input = new FileInputStream(file);
        MultipartFile image = new MockMultipartFile("profile-pic.png", file.getName(),
                                                "image/png", IOUtils.toByteArray(input));
        return image;
    }

    @Test
    void findImageById() throws Exception{
        Mockito.when(imageMongo.findById(123L)).thenReturn(Optional.of(image));
        Optional<ImageModelMongo> image = imageMongo.findById(123L);
        MultipartFile imageFile = createImage();
        String imageEncode = Base64.getEncoder().encodeToString(imageFile.getBytes());
        assertEquals(123L, image.get().getId());
        assertEquals(imageEncode, image.get().getImage());
    }

}
